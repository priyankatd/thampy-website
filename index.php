<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Thampy Digital - Creative Digital Agency based in Dubai, UAE | Cochin, India | London, United Kingdom">
    <meta name="keywords" content="Thampy Digital - Creative Digital Agency based in Dubai, UAE | Cochin, India | London, United Kingdom">
    <meta name="author" content="Thampy Digital">
    <title>Thampy Digital - Creative Digital Agency based in Dubai, UAE | Cochin, India | London, United Kingdom</title>
    <link rel="shortcut icon" href="assets/images/favicon.png">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/custom/main.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body dir="ltr">
    <?php include("config.php"); ?>
    <div class="home">
      <div class="clearfix">
      
        <!-- Home page main banner -->
        <div class="home__banner">
          <video autoplay muted loop poster="polina.jpg" id="bgvid" src="http://vjs.zencdn.net/v/oceans.mp4">
              <!-- <source src="polina.webm" type="video/webm">
              <source src="polina.mp4" type="video/mp4"> -->
          </video>
        </div>
        <!-- Home page main banner ends -->
        
        <!-- Home page work display -->
          <div class="clearfix">
            <ul class="home__work-section">
              <li class="col-md-4 col-sm-6">
                <div class="home__work-section--work1">
                  <div class="col-md-8 col-md-offset-2">
                    <h4>SINCE 2010 WE HAVE HELPED MANY BRANDS ACROSS.</h4>
                    <hr/>
                    <p>SINCE 2010 WE HAVE</p>
                  </div>
                </div>
              </li>
              <li class="col-md-4 col-sm-6">
                <div class="home__work-section--work1">
                  <div class="col-md-8 col-md-offset-2">
                    <h4>SINCE 2010 WE HAVE HELPED MANY BRANDS ACROSS.</h4>
                    <hr/>
                    <p>SINCE 2010 WE HAVE</p>
                  </div>
                </div>
              </li>
              <li class="col-md-4 col-sm-6">
                <div class="home__work-section--work1">
                  <div class="col-md-8 col-md-offset-2">
                    <h4>SINCE 2010 WE HAVE HELPED MANY BRANDS ACROSS.</h4>
                    <hr/>
                    <p>SINCE 2010 WE HAVE</p>
                  </div>
                </div>
              </li>
              <li class="col-md-12 col-sm-6">
                <div class="home__work-section--work1">
                  <div class="col-md-4 col-md-offset-4">
                    <h4>SINCE 2010 WE HAVE HELPED MANY BRANDS ACROSS.</h4>
                    <hr/>
                    <p>SINCE 2010 WE HAVE</p>
                  </div>
                </div>
              </li>
            </ul>
          </div>
          <!-- Home page about description -->
          <div class="clearfix">
            <div class="home__about"><h1>About</h1></div>
          </div>
          <!-- Home page about description ends-->

          <!--Home page tabs to other page -->
          <div class="clearfix">
            <ul class="home__tabs">
              <li class="col-md-4 col-sm-6"><h1>Tab1</h1></li>
              <li class="col-md-4 col-sm-6"><h1>Tab2</h1></li>
              <li class="col-md-4 col-sm-6"><h1>Tab3</h1></li>
            </ul>
          </div>
          <!--Home page tabs to other page ends -->
        
        </div>
        <!-- Home page work display ends -->
      </div>
    </div>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="assets/js/vendor.min.js"></script>
    <script src="assets/js/custom.min.js"></script>
  </body>
</html>